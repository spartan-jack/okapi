package net.sf.okapi.filters.openxml;

import java.util.zip.ZipEntry;

import javax.xml.stream.events.XMLEvent;

class ExcelCommentPartHandler extends StyledTextPartHandler {
	ExcelCommentPartHandler(ConditionalParameters cparams, OpenXMLZipFile zipFile, ZipEntry entry,
		   StyleDefinitions styleDefinitions, StyleOptimisation styleOptimisation) {
		super(cparams, zipFile, entry, styleDefinitions, styleOptimisation);
	}

	@Override
	protected boolean isStyledBlockStartEvent(XMLEvent e) {
		return XMLEventHelpers.isStartElement(e, "text");
	}
}
