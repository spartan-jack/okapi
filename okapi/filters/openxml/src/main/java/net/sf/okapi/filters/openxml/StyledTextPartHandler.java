/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.SECTION_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_CELL_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_GRID_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_PROPERTIES_EXCEPTIONS_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_ROW_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createEndMarkupComponent;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createGeneralMarkupComponent;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createStartMarkupComponent;
import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isBlockMarkupEndEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isBlockMarkupStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isSectionPropertiesStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTableGridEndEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTableGridStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTablePropertiesStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTableRowStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTextBodyPropertiesStartEvent;

/**
 * Part handler for styled text (Word document parts, PPTX slides) that
 * follow the styled run model.
 */
class StyledTextPartHandler extends GenericPartHandler {
	protected final StyleDefinitions styleDefinitions;
	protected final StyleOptimisation styleOptimisation;
	protected final IdGenerator nestedBlockId;
	protected final IdGenerator textUnitId;

	private StrippableAttributes.TableRowRevisions tableRowRevisions;
	private StrippableAttributes.SectionPropertiesRevisions sectionPropertiesRevisions;

	private ElementSkipper tablePropertiesChangeElementSkipper;
	private ElementSkipper noElementSkipper;
	private ElementSkipper revisionPropertyChangeElementSkipper;

	protected XMLEventReader xmlReader;

	protected Iterator<Event> filterEventIterator;
	protected String documentId;
	protected String subDocumentId;
	protected LocaleId sourceLocale;

	protected Markup markup;

	StyledTextPartHandler(
			final ConditionalParameters cparams,
			final OpenXMLZipFile zipFile,
			final ZipEntry entry,
			final StyleDefinitions styleDefinitions,
			final StyleOptimisation styleOptimisation) {
		super(cparams, zipFile, entry);
		this.styleDefinitions = styleDefinitions;
		this.styleOptimisation = styleOptimisation;
		this.nestedBlockId = new IdGenerator(null);
		this.textUnitId = new IdGenerator(entry.getName(), IdGenerator.TEXT_UNIT);

		this.markup = new Block.BlockMarkup();
	}

	/**
	 * Open this part and perform any initial processing.  Return the
	 * first event for this part.  In this case, it's a START_SUBDOCUMENT
	 * event.
	 *
	 * @param documentId document identifier
	 * @param subDocumentId sub-document identifier
	 * @param sourceLocale the locale of the source
	 *
	 * @return Event
	 *
	 * @throws IOException
	 * @throws XMLStreamException
     */
	@Override
	public Event open(String documentId, String subDocumentId, LocaleId sourceLocale) throws IOException,
			XMLStreamException {
		this.documentId = documentId;
		this.subDocumentId = subDocumentId;
		this.sourceLocale = sourceLocale;

		/**
		 * Process the XML event stream, simplifying as we go.  Non-block content is
		 * written as a document part.  Blocks are parsed, then converted into TextUnit structures.
		 */
		xmlReader = zipFile.getInputFactory().createXMLEventReader(
				new InputStreamReader(new BufferedInputStream(zipFile.getInputStream(entry)), StandardCharsets.UTF_8));
		return open(documentId, subDocumentId, xmlReader);
	}

	// Package-private for test.  XXX This is an artifact of the overall PartHandler
	// interface needing work.
	Event open(String documentId, String subDocumentId, XMLEventReader xmlReader) throws XMLStreamException {
		this.xmlReader = xmlReader;

		tableRowRevisions = new StrippableAttributes.TableRowRevisions();
		sectionPropertiesRevisions = new StrippableAttributes.SectionPropertiesRevisions();

		tablePropertiesChangeElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(this.params, TABLE_PROPERTIES_CHANGE);
		noElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(this.params);
		revisionPropertyChangeElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
				this.params,
				SECTION_PROPERTIES_CHANGE,
				TABLE_GRID_CHANGE,
				TABLE_PROPERTIES_EXCEPTIONS_CHANGE,
				TABLE_ROW_PROPERTIES_CHANGE,
				TABLE_CELL_PROPERTIES_CHANGE);

		try {
			process();
		}
		finally {
			if (xmlReader != null) {
				xmlReader.close();
			}
		}
		return createStartSubDocumentEvent(documentId, subDocumentId);
	}

	/**
	 * Check to see if the current element starts a block.  Can be overridden.
	 */
	protected boolean isStyledBlockStartEvent(XMLEvent e) {
		return XMLEventHelpers.isParagraphStartEvent(e);
	}

	private void process() throws XMLStreamException {
		StartElement parentStartElement = null;

		while (xmlReader.hasNext()) {
			XMLEvent e = xmlReader.nextEvent();
			preHandleNextEvent(e);

			if (isStyledBlockStartEvent(e)) {
				flushDocumentPart();
				StartElementContext startElementContext =
						createStartElementContext(e.asStartElement(), xmlReader, zipFile.getEventFactory(), params, sourceLocale);
				Block block = new BlockParser(startElementContext, nestedBlockId, styleDefinitions, styleOptimisation).parse();
				if (block.isHidden()) {
					documentPartEvents.addAll(block.getEvents());
					continue;
				}

				final List<ITextUnit> textUnits = new BlockTextUnitMapper(block, textUnitId).map();
				if (textUnits.isEmpty() || !isCurrentBlockTranslatable()) {
					addBlockChunksToDocumentPart(block.getChunks());
				}
				else {
					for (ITextUnit tu : textUnits) {
						filterEvents.add(new Event(EventType.TEXT_UNIT, tu));
					}
				}
			}
			else if (isBlockMarkupStartEvent(e)) {
				addMarkupComponentToDocumentPart(createStartMarkupComponent(zipFile.getEventFactory(), e.asStartElement()));
			}
			else if (isTablePropertiesStartEvent(e)) {
				StartElementContext startElementContext = createStartElementContext(e.asStartElement(), xmlReader, zipFile.getEventFactory(), params);
				addMarkupComponentToDocumentPart(MarkupComponentParser.parseBlockProperties(startElementContext, tablePropertiesChangeElementSkipper));
			}
			else if (isTextBodyPropertiesStartEvent(e)) {
				StartElementContext startElementContext = createStartElementContext(e.asStartElement(), xmlReader, zipFile.getEventFactory(), params);
				addMarkupComponentToDocumentPart(MarkupComponentParser.parseBlockProperties(startElementContext, noElementSkipper));
			}
			else if (isBlockMarkupEndEvent(e)) {
				addMarkupComponentToDocumentPart(createEndMarkupComponent(e.asEndElement()));
			}
			else if (e.isStartElement() && revisionPropertyChangeElementSkipper.canSkip(e.asStartElement(), parentStartElement)) {
				StartElementContext startElementContext = createStartElementContext(e.asStartElement(), parentStartElement, xmlReader, zipFile.getEventFactory(), params, null);
				revisionPropertyChangeElementSkipper.skip(startElementContext);
			}
			else {
				if (isSectionPropertiesStartEvent(e)) {
					e = sectionPropertiesRevisions.strip(createStartElementContext(e.asStartElement(), null, zipFile.getEventFactory(), params));
				} else if (isTableRowStartEvent(e)) {
					e = tableRowRevisions.strip(createStartElementContext(e.asStartElement(), null, zipFile.getEventFactory(), params));
				}
				addEventToDocumentPart(e);
			}

			if (isTableGridStartEvent(e)) {
				parentStartElement = e.asStartElement();
			} else if (isTableGridEndEvent(e)) {
				parentStartElement = null;
			}
		}
		flushDocumentPart();
		filterEvents.add(new Event(EventType.END_DOCUMENT, new Ending(subDocumentId)));
		filterEventIterator = filterEvents.iterator();
	}

	protected void flushDocumentPart() {
		if (!documentPartEvents.isEmpty()) {
			markup.addComponent(createGeneralMarkupComponent(documentPartEvents));
			documentPartEvents = new ArrayList<>();
		}

		if (!markup.getComponents().isEmpty()) {
			DocumentPart documentPart = new DocumentPart(documentPartIdGenerator.createId(), false);
			documentPart.setSkeleton(new MarkupSkeleton(markup));
			markup = new Block.BlockMarkup();

			filterEvents.add(new Event(EventType.DOCUMENT_PART, documentPart));
		}
	}

	protected void preHandleNextEvent(XMLEvent e) {
		// can be overridden
	}

	protected boolean isCurrentBlockTranslatable() {
		// can be overridden
		// here blocks are always translatable
		return true;
	}

	protected void addMarkupComponentToDocumentPart(MarkupComponent markupComponent) {
		if (!documentPartEvents.isEmpty()) {
			markup.addComponent(createGeneralMarkupComponent(documentPartEvents));
			documentPartEvents = new ArrayList<>();
		}
		markup.addComponent(markupComponent);
	}

	protected void addBlockChunksToDocumentPart(List<Chunk> chunks) {
		for (Chunk chunk : chunks) {
			if (chunk instanceof Markup) {
				for (MarkupComponent markupComponent : ((Markup) chunk).getComponents()) {
					addMarkupComponentToDocumentPart(markupComponent);
				}
				continue;
			}

			documentPartEvents.addAll(chunk.getEvents());
		}
	}

	@Override
	public boolean hasNext() {
		return filterEventIterator.hasNext();
	}

	@Override
	public Event next() {
		return filterEventIterator.next();
	}

	@Override
	public void close() {
	}

	@Override
	public void logEvent(Event e) {
	}
}
