/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

/**
 * Provides XML schema definition.
 */
final class SchemaDefinition {
    private static final String COMPONENT_NAME_DOES_NOT_EXIST = "The component name does not exist";
    private static final String EMPTY = "";

    enum Composition {
        SEQUENCE("Sequence"),
        CHOICE("Choice"),
        ALL("All"),
        NONE("");

        private final String type;

        Composition(final String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return this.type;
        }
    }

    interface Component {

        QName name();

        Composition composition();

        ListIterator<Component> listIterator();

        default ListIterator<Component> listIteratorAfter(final QName name) {
            final ListIterator<Component> iterator = this.listIterator();
            boolean found = false;
            while (iterator.hasNext()) {
                final Component component = iterator.next();
                if (component.name().equals(name)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new IllegalArgumentException(COMPONENT_NAME_DOES_NOT_EXIST);
            }
            return iterator;
        }
    }

    static final class Element implements Component {
        private final QName name;

        Element(final QName name) {
            this.name = name;
        }

        @Override
        public QName name() {
            return this.name;
        }

        @Override
        public Composition composition() {
            return Composition.NONE;
        }

        @Override
        public ListIterator<Component> listIterator() {
            return java.util.Collections.emptyListIterator();
        }
    }

    static final class Group implements Component {
        private final QName name;
        private final Composition composition;
        private final List<Component> components;

        Group(final QName name, final Composition composition, final Component... components) {
            this.name = name;
            this.composition = composition;
            this.components = Arrays.asList(components);
        }

        @Override
        public QName name() {
            return this.name;
        }

        @Override
        public Composition composition() {
            return this.composition;
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.components.listIterator();
        }
    }

    static final class FillProperties implements Component {
        private final Group group;

        FillProperties(final String prefix) {
            this.group = new Group(
                Namespaces.Empty.getQName(EMPTY),
                Composition.CHOICE,
                new Element(Namespaces.DrawingML.getQName("noFill", prefix)),
                new Element(Namespaces.DrawingML.getQName("solidFill", prefix)),
                new Element(Namespaces.DrawingML.getQName("gradFill", prefix)),
                new Element(Namespaces.DrawingML.getQName("blipFill", prefix)),
                new Element(Namespaces.DrawingML.getQName("pattFill", prefix)),
                new Element(Namespaces.DrawingML.getQName("grpFill", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class EffectProperties implements Component {
        private final Group group;

        EffectProperties(final String prefix) {
            this.group = new Group(
                Namespaces.Empty.getQName(EMPTY),
                Composition.CHOICE,
                new Element(Namespaces.DrawingML.getQName("effectLst", prefix)),
                new Element(Namespaces.DrawingML.getQName("effectDag", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextUnderlineLine implements Component {
        private final Group group;

        TextUnderlineLine(final String prefix) {
            this.group = new Group(
                Namespaces.Empty.getQName(EMPTY),
                Composition.CHOICE,
                new Element(Namespaces.DrawingML.getQName("uLnTx", prefix)),
                new Element(Namespaces.DrawingML.getQName("uLn", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextUnderlineFill implements Component {
        private final Group group;

        TextUnderlineFill(final String prefix) {
            this.group = new Group(
                Namespaces.Empty.getQName(EMPTY),
                Composition.CHOICE,
                new Element(Namespaces.DrawingML.getQName("uFillTx", prefix)),
                new Element(Namespaces.DrawingML.getQName("uFill", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextBulletColor implements Component {
        private final Group group;

        TextBulletColor(final String prefix) {
            this.group = new Group(
                Namespaces.Empty.getQName(EMPTY),
                Composition.CHOICE,
                new Element(Namespaces.DrawingML.getQName("buClrTx", prefix)),
                new Element(Namespaces.DrawingML.getQName("buClr", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextBulletSize implements Component {
        private final Group group;

        TextBulletSize(final String prefix) {
            this.group = new Group(
                    Namespaces.Empty.getQName(EMPTY),
                    Composition.CHOICE,
                    new Element(Namespaces.DrawingML.getQName("buSzTx", prefix)),
                    new Element(Namespaces.DrawingML.getQName("buSzPct", prefix)),
                    new Element(Namespaces.DrawingML.getQName("buSzPsts", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextBulletTypeface implements Component {
        private final Group group;

        TextBulletTypeface(final String prefix) {
            this.group = new Group(
                Namespaces.Empty.getQName(EMPTY),
                Composition.CHOICE,
                new Element(Namespaces.DrawingML.getQName("buFontTx", prefix)),
                new Element(Namespaces.DrawingML.getQName("buFont", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextBullet implements Component {
        private final Group group;

        TextBullet(final String prefix) {
            this.group = new Group(
                Namespaces.Empty.getQName(EMPTY),
                Composition.CHOICE,
                new Element(Namespaces.DrawingML.getQName("buNone", prefix)),
                new Element(Namespaces.DrawingML.getQName("buAutoNum", prefix)),
                new Element(Namespaces.DrawingML.getQName("buChar ", prefix)),
                new Element(Namespaces.DrawingML.getQName("buBlip ", prefix))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextCharacterProperties implements Component {
        private final Group group;

        TextCharacterProperties(final QName name) {
            this.group = new Group(
                name, Composition.SEQUENCE,
                new Element(Namespaces.DrawingML.getQName("ln", name.getPrefix())),
                new FillProperties(name.getPrefix()),
                new EffectProperties(name.getPrefix()),
                new Element(Namespaces.DrawingML.getQName("highlight", name.getPrefix())),
                new TextUnderlineLine(name.getPrefix()),
                new TextUnderlineFill(name.getPrefix()),
                new Element(Namespaces.DrawingML.getQName("latin", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("ea", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("cs", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("sym", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("hlinkClick", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("hlinkMouseOver", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("rtl", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("extList", name.getPrefix()))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return this.group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }

    static final class TextParagraphProperties implements Component {
        private final Group group;

        TextParagraphProperties(final QName name) {
            this.group = new Group(
                name,
                Composition.SEQUENCE,
                new Element(Namespaces.DrawingML.getQName("lnSpc", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("spcBef", name.getPrefix())),
                new Element(Namespaces.DrawingML.getQName("spcAlt", name.getPrefix())),
                new TextBulletColor(name.getPrefix()),
                new TextBulletSize(name.getPrefix()),
                new TextBulletTypeface(name.getPrefix()),
                new TextBullet(name.getPrefix()),
                new Element(Namespaces.DrawingML.getQName("tabLst", name.getPrefix())),
                new TextCharacterProperties(
                        Namespaces.DrawingML.getQName("defRPr", name.getPrefix())
                ),
                new Element(Namespaces.DrawingML.getQName("extLst", name.getPrefix()))
            );
        }

        @Override
        public QName name() {
            return this.group.name();
        }

        @Override
        public Composition composition() {
            return group.composition();
        }

        @Override
        public ListIterator<Component> listIterator() {
            return this.group.listIterator();
        }
    }
}
